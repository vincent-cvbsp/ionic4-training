@echo off
COLOR 7
set keystoreFilename=prod.keystore

call ionic cordova build --prod --release android
if %errorlevel% neq 0 (
    COLOR 4
    exit /b %errorlevel% "ERREUR "
)

call copy ".\_release\%keystoreFilename%" ".\platforms\android\app\build\outputs\apk\release\%keystoreFilename%"
if %errorlevel% neq 0 (
    COLOR 4
    exit /b %errorlevel% "ERREUR "
)


call "C:\Program Files\Java\jdk1.8.0_191\bin\jarsigner" -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore C:\works\%dirApp%\platforms\android\app\build\outputs\apk\release\%keystoreFilename% C:\works\%dirApp%\platforms\android\app\build\outputs\apk\release\app-release-unsigned.apk vincent
::PASSWORD WILL BE ASKED HERE
if %errorlevel% neq 0 (
    COLOR 4
    exit /b %errorlevel% "ERREUR "
)

call C:\Users\ptit_\AppData\Local\Android\Sdk\build-tools\28.0.3\zipalign -v 4  C:\works\%dirApp%\platforms\android\app\build\outputs\apk\release\app-release-unsigned.apk  C:\works\%dirApp%\platforms\android\app\build\outputs\apk\release\generic.apk
if %errorlevel% neq 0 (
    COLOR 4
    exit /b %errorlevel% "ERREUR "
)

call copy ".\platforms\android\app\build\outputs\apk\release\prod.apk" ".\_release\prod.apk"
if %errorlevel% neq 0 (
    COLOR 4
    exit /b %errorlevel% "ERREUR "
) 
